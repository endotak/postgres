# Postgresql & PgAdmin


## Requirements:
* docker >= 17.12.0+
* docker-compose

## Quick Start
* Clone or download this repository
* Go inside of directory,  `cd postgres`
* Copy .env.example to .env `cp .env.example .env`
* Override .env variables as your need
* Run this command `docker-compose up -d`

## Access to postgres:
* `localhost:5432`
* **Username:** admin (as a default)
* **Password:** admin (as a default)

## Access to PgAdmin:
* **URL:** `http://localhost:8080`
* **Username:** example@example.com (as a default)
* **Password:** admin (as a default)

## Add a new server in PgAdmin:
* **Host name/address** `postgres`
* **Port** `5432`
* **Username** as `POSTGRES_USER`, by default: `admin`
* **Password** as `POSTGRES_PASSWORD`, by default `admin`
